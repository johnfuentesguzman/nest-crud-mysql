## Description

This is a [Nest](https://github.com/nestjs/nest) repository resultant of the NestJS Crud + Login 

https://www.youtube.com/watch?v=eL2tO9xPZLM 

# Docs
https://docs.nestjs.com/techniques/database

## Content
- NestJS Controller and Dependency Injection
- TypeORM Integration with MySQL database
- @nestjs/config package integration for Environment Variables handling
- Secure password encryption with BcryptJs 
- Authentication strategies with Passport
- Custom Decorator and tips
- nest-access-control integration for RBAC and ABAC introduction
- Class validator with custom settings
- On boot admin user generation
- ETC.


## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## git repo
https://github.com/ruslanguns/nestjs-myblog

## tutorial video
https://www.youtube.com/watch?v=eL2tO9xPZLM&list=PLiWSP2AKEiyD1Y8MgErrrEuQEGUUpHs9L&index=1&t=8383s